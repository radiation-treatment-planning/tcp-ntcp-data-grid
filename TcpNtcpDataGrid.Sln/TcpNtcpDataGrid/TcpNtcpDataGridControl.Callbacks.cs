﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TcpNtcpCalculation.Helpers;
using Prism.Mvvm;
using Prism.Commands;

namespace TcpNtcpDataGrid
{
    public partial class TcpNtcpDataGridControl
    {
        private static void ParetoDataPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (TcpNtcpDataGridControl)d;
            control.ParetoData = e.NewValue as Pareto.Pareto;
            control.UpdateTcpNtcpCftcDataGridRows(control.ParetoData);
        }

        private static void ClinicalParetoPointIdPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (TcpNtcpDataGridControl) d;
            control.ClinicalParetoPointId = e.NewValue as string;
            control.UpdateTcpNtcpCftcDataGridRows(control.ParetoData);
        }

    }
}
