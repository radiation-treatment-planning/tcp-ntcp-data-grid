﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using TcpNtcpCalculation.Helpers;
using Prism.Mvvm;
using Prism.Commands;
using TcpNtcpDataGrid.Extractor;

namespace TcpNtcpDataGrid
{
    public partial class TcpNtcpDataGridControl
    {
        private void UpdateTcpNtcpCftcDataGridRows(Pareto.Pareto newParetoData)
        {
            var clinicalPlan = GetClinicalPlan(newParetoData);
            var globalOptimum = GetGlobalOptimum(newParetoData.Points);
            var illusionPoint = GetIllusionPoint(newParetoData.Points);
            var allOtherPoints = newParetoData.Points.Except(new List<ParetoPoint>{clinicalPlan, globalOptimum, illusionPoint});
            if (!allOtherPoints.Any())
                throw new ArgumentException($"There must exist any pareto points to calculate a data grid.");
            TcpNtcpCftcDataGridRows = GenerateDataGridRows(clinicalPlan, globalOptimum, illusionPoint, allOtherPoints);
        }

        private ObservableCollection<TcpNtcpCftcDataGridRow> GenerateDataGridRows(ParetoPoint clinicalPlan, ParetoPoint globalOptimum, ParetoPoint illusionPoint, IEnumerable<ParetoPoint> allOtherPoints)
        {
            var globalOptimumProps = ReadTcpNtcpCftcInformationFromParetoTag(globalOptimum.Tag.ToString());
            var illusionPointProps = ReadTcpNtcpCftcInformationFromParetoTag(illusionPoint.Tag.ToString());
            var allOtherPointsProps =
                allOtherPoints.Select(x => ReadTcpNtcpCftcInformationFromParetoTag(x.Tag.ToString()));
            var planWithMaximumCftc = allOtherPointsProps.OrderByDescending(x => x.CFTC).First();
            var rows = new ObservableCollection<TcpNtcpCftcDataGridRow>();
            if (clinicalPlan == null)
            {
                rows.Add(new TcpNtcpCftcDataGridRow("TCP", "N/A", planWithMaximumCftc.TCP, globalOptimumProps.TCP, illusionPointProps.TCP));
                rows.Add(new TcpNtcpCftcDataGridRow("NTCP", "N/A", planWithMaximumCftc.NTCP, globalOptimumProps.NTCP, illusionPointProps.NTCP));
                rows.Add(new TcpNtcpCftcDataGridRow("CFTC", "N/A", planWithMaximumCftc.CFTC, globalOptimumProps.CFTC, illusionPointProps.CFTC));
            }
            else
            {
                var clinicalPlanProps = ReadTcpNtcpCftcInformationFromParetoTag(clinicalPlan.Tag.ToString());
                rows.Add(new TcpNtcpCftcDataGridRow("TCP", clinicalPlanProps.TCP.ToString(), planWithMaximumCftc.TCP, globalOptimumProps.TCP, illusionPointProps.TCP));
                rows.Add(new TcpNtcpCftcDataGridRow("NTCP", clinicalPlanProps.NTCP.ToString(), planWithMaximumCftc.NTCP, globalOptimumProps.NTCP, illusionPointProps.NTCP));
                rows.Add(new TcpNtcpCftcDataGridRow("CFTC", clinicalPlanProps.CFTC.ToString(), planWithMaximumCftc.CFTC, globalOptimumProps.CFTC, illusionPointProps.CFTC));
            }
            return rows;
        }

        private ParetoPoint GetIllusionPoint(IEnumerable<ParetoPoint> points)
        {
            var type = ParetoPoint.Factory
                .CreateIllusionParetoPoint("", Point2D.Origin, OxyColors.AliceBlue, MarkerType.Circle).GetType();
            var illusionPoint = points.FirstOrDefault(x => x.GetType() == type);
            if (illusionPoint == null) throw new ArgumentException($"No illusion point found in {nameof(points)}");
            return illusionPoint;
        }

        private ParetoPoint GetGlobalOptimum(IEnumerable<ParetoPoint> points)
        {
            var type = ParetoPoint.Factory
                .CreateGlobalOptimumParetoPoint("", Point2D.Origin, OxyColors.AliceBlue, MarkerType.Circle).GetType();
            var globalOptimum = points.FirstOrDefault(x => x.GetType() == type);
            if (globalOptimum == null) throw new ArgumentException($"No global optimum found in {nameof(points)}");
            return globalOptimum;
        }

        private ParetoPoint GetClinicalPlan(Pareto.Pareto newParetoData)
        {
            if (ClinicalParetoPointId == null) return null;
            foreach (var paretoPoint in newParetoData.Points)
            {
                var paretoPointProperties = ReadTcpNtcpCftcInformationFromParetoTag(paretoPoint.Tag.ToString());
                if (paretoPointProperties.PlanId == ClinicalParetoPointId) return paretoPoint;
            }

            return null;
        }

        private TcpNtcpTagExtractorResult ReadTcpNtcpCftcInformationFromParetoTag(
            string tagDescription)
        {
            return _cftcInformationFromParetoTagExtractor.Extract(tagDescription);
        }


    }
}
