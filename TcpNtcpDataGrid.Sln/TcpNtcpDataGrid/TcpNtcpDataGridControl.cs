﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TcpNtcpCalculation.Helpers;
using Prism.Mvvm;
using Prism.Commands;

namespace TcpNtcpDataGrid
{
    public partial class TcpNtcpDataGridControl : Control
    {
        private static readonly TcpNtcpCftcInformationFromParetoTagExtractor _cftcInformationFromParetoTagExtractor;
        static TcpNtcpDataGridControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TcpNtcpDataGridControl), new FrameworkPropertyMetadata(typeof(TcpNtcpDataGridControl)));
            _cftcInformationFromParetoTagExtractor = new TcpNtcpCftcInformationFromParetoTagExtractor(2);
        }

        internal ObservableCollection<TcpNtcpCftcDataGridRow> TcpNtcpCftcDataGridRows
        {
            get { return (ObservableCollection<TcpNtcpCftcDataGridRow>)GetValue(TcpNtcpCftcDataGridRowsProperty); }
            private set { SetValue(TcpNtcpCftcDataGridRowsProperty, value); }
        }

        public static readonly DependencyProperty TcpNtcpCftcDataGridRowsProperty =
            DependencyProperty.Register("TcpNtcpCftcDataGridRows", typeof(ObservableCollection<TcpNtcpCftcDataGridRow>),
                typeof(TcpNtcpDataGridControl), new PropertyMetadata(new ObservableCollection<TcpNtcpCftcDataGridRow>()));

        public Pareto.Pareto ParetoData
        {
            get { return (Pareto.Pareto)GetValue(ParetoDataProperty); }
            set { SetValue(ParetoDataProperty, value); }
        }

        public static readonly DependencyProperty ParetoDataProperty =
            DependencyProperty.Register("ParetoData", typeof(Pareto.Pareto),
                typeof(TcpNtcpDataGridControl), new PropertyMetadata(null, ParetoDataPropertyChangedCallback));

        public string ClinicalParetoPointId
        {
            get { return (string)GetValue(ClinicalParetoPointIdProperty); }
            set { SetValue(ClinicalParetoPointIdProperty, value); }
        }

        public static readonly DependencyProperty ClinicalParetoPointIdProperty =
            DependencyProperty.Register("ClinicalParetoPointId", typeof(string), typeof(TcpNtcpDataGridControl),
                new PropertyMetadata(null, ClinicalParetoPointIdPropertyChangedCallback));

    }
}
