﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpNtcpDataGrid
{
    public class TcpNtcpCftcDataGridRow
    {
        public string RowAttributeName { get; }
        public string ClinicalPlanValue { get; }
        public double ComplicationFreeTumorControlValue { get; }
        public double PatientGlobalOptimumValue { get; }
        public double IllusionPointValue { get; }

        public TcpNtcpCftcDataGridRow(string rowAttributeName, string clinicalPlanValue, double complicationFreeTumorControlValue, double patientGlobalOptimumValue, double illusionPointValue)
        {
            RowAttributeName = rowAttributeName;
            ClinicalPlanValue = clinicalPlanValue;
            ComplicationFreeTumorControlValue = complicationFreeTumorControlValue;
            PatientGlobalOptimumValue = patientGlobalOptimumValue;
            IllusionPointValue = illusionPointValue;
        }
    }

}
