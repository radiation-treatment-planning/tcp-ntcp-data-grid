﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpNtcpDataGrid.Extractor
{
    public class TcpNtcpTagExtractorResult
    {
        public string PlanId { get; }
        public double TCP { get; }
        public double NTCP { get; }
        public double CFTC { get; }

        public TcpNtcpTagExtractorResult(string planId, double tcp, double ntcp, double cftc)
        {
            PlanId = planId;
            TCP = tcp;
            NTCP = ntcp;
            CFTC = cftc;
        }
    }
}
