﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TcpNtcpDataGrid.Extractor;

namespace TcpNtcpDataGrid
{
    public class TcpNtcpCftcInformationFromParetoTagExtractor
    {
        private readonly uint _numberOfFloatingPointDigits;
        private readonly IFormatProvider _formatProvider;

        public TcpNtcpCftcInformationFromParetoTagExtractor(uint numberOfFloatingPointDigits, IFormatProvider formatProvider=null)
        {
            _numberOfFloatingPointDigits = numberOfFloatingPointDigits;
            _formatProvider = formatProvider ?? new NumberFormatInfo() {NumberDecimalSeparator = ".", NumberGroupSeparator = ","};
        }

        public TcpNtcpTagExtractorResult Extract(
            string tagDescription)
        {
            var rows = tagDescription.Split('\n');

            var planIdPrefix = "Plan Id:\t";
            var tcpPrefix = "TCP:\t";
            var ntcpPrefix = "NTCP:\t";
            var cftcPrefix = "P+:\t";

            string planId = null;
            double tcp = -1;
            double ntcp = -1;
            double cftc = -1;
            foreach (var row in rows)
            {
                if (row.StartsWith(planIdPrefix))
                    planId = row.Replace(planIdPrefix, "");
                else if (row.StartsWith(tcpPrefix))
                    tcp = CastToRoundedDouble(row.Replace(tcpPrefix, ""));
                else if (row.StartsWith(ntcpPrefix))
                    ntcp = CastToRoundedDouble(row.Replace(ntcpPrefix, ""));
                else if (row.StartsWith(cftcPrefix))
                    cftc = CastToRoundedDouble(row.Replace(cftcPrefix, ""));
            }
            return new TcpNtcpTagExtractorResult(planId, tcp, ntcp, cftc);
        }

        private double CastToRoundedDouble(string number)
        {
            double convertedNumber;
            if (!double.TryParse(s: number, NumberStyles.AllowDecimalPoint, _formatProvider, out convertedNumber))
                throw new ArgumentException($"Cannot cast {number} to double.");
            return Math.Round(convertedNumber, (int)_numberOfFloatingPointDigits);
        }
    }
}
