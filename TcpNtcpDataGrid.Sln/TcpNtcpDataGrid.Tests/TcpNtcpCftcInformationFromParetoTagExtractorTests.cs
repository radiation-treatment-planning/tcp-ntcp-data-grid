﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;

namespace TcpNtcpDataGrid.Tests
{
    [TestFixture]
    public class TcpNtcpCftcInformationFromParetoTagExtractorTests
    {
        [Test]
        public void Extract_Test()
        {
            var cftcInformationFromParetoTagExtractor = new TcpNtcpCftcInformationFromParetoTagExtractor(2);
            var paretoPoint = ParetoPoint.Factory.CreateParetoPoint(
                ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                {
                    ("Patient Piz", "12345"), ("Plan Id", "plan1"), ("TCP", "0.912345"), ("NTCP", "0.12345"),
                    ("P+", "0.79971600975")
                }), Point2D.Origin, OxyColors.AliceBlue, MarkerType.Circle);
            var result = cftcInformationFromParetoTagExtractor.Extract(paretoPoint.Tag.ToString());
            var tolerance = 0.01;
            Assert.AreEqual("plan1", result.PlanId);
            Assert.AreEqual(0.912345, result.TCP, tolerance);
            Assert.AreEqual(0.12345, result.NTCP, tolerance);
            Assert.AreEqual(0.79971600975, result.CFTC, tolerance);
        }
    }
}
