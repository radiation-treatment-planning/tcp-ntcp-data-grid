﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using Prism.Mvvm;

namespace ExampleApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Example Data Grid App";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Pareto.Pareto _customParetoData;
        public Pareto.Pareto CustomParetoData
        {
            get { return _customParetoData; }
            set { SetProperty(ref _customParetoData, value); }
        }

        private string _customClinicalParetoPointId;
        public string CustomClinicalParetoPointId
        {
            get { return _customClinicalParetoPointId; }
            set { SetProperty(ref _customClinicalParetoPointId, value); }
        }

        public MainWindowViewModel()
        {
            CustomParetoData = new Pareto.Pareto(new ParetoPoint[]
                {
                    ParetoPoint.Factory.CreateGlobalOptimumParetoPoint(
                        ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                            {("TCP", "0.9"), ("NTCP", "0.1"), ("P+", "0.81")}), new Point2D(0.9, 0.1),
                        OxyColors.AliceBlue, MarkerType.Circle),
                    ParetoPoint.Factory.CreateIllusionParetoPoint(
                        ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                            {("TCP", "0.99"), ("NTCP", "0.01"), ("P+", "0.9801")}), new Point2D(0.99, 0.01),
                        OxyColors.AliceBlue, MarkerType.Circle),
                    ParetoPoint.Factory.CreateParetoPoint(
                        ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                            {("TCP", "0.8"), ("NTCP", "0.3"), ("P+", "0.56")}), new Point2D(0.8, 0.3),
                        OxyColors.AliceBlue, MarkerType.Circle),
                    ParetoPoint.Factory.CreateParetoPoint(
                        ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                            {("TCP", "0.7"), ("NTCP", "0.4"), ("P+", "0.42")}), new Point2D(0.7, 0.4),
                        OxyColors.AliceBlue, MarkerType.Circle),
                    ParetoPoint.Factory.CreateUniqueParetoPoint(
                        ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                            {("Plan Id", "Clinical Plan"),("TCP", "0.7"), ("NTCP", "0.4"), ("P+", "0.42")}), new Point2D(0.7, 0.4),
                        OxyColors.AliceBlue, MarkerType.Circle, "Clinical Plan"),
                },
                new ParetoLine[] { });
            CustomClinicalParetoPointId = "Clinical Plan";
        }
    }
}
